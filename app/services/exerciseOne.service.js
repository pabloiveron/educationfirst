const service = {};

/**
 * @description Generates an array of random numbers. Size of array is parameter size + 1
 * @param size Size - 1 of generated array.
 * @return Array of random numbers.
 */
service.generateArray = (size) => {
  if (!size) return null;
  return Array.from({length: size + 1}, () => Math.floor(Math.random() * size + 1));
}

/**
 * @description Find duplicated values in an array of numbers
 * @param arr Array of numbers.
 * @return Array of duplicated numbers.
 */
service.findDuplicates = (arr) => {
  const result = [];
  // We do sort as we don't care about modifying the arr parameter.
  arr.sort().forEach((e, i) => {
    if (arr[i - 1] === e && result[result.length - 1] !== e) {
      result.push(e);
    }
  });
  return result;
}

module.exports = service;