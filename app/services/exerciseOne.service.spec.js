const service = require('./exerciseOne.service');

describe('exerciseOne.service', () => {
  describe('generateArray', () => {

    let newArray;

    beforeAll(() => {
      newArray = service.generateArray(12);
    });

    it('should generate an array ', () => {
      expect(newArray).toBeDefined();
      expect(newArray.length).toBeGreaterThan(1);
    });

    it('should be size + 1', () => {
      expect(newArray.length).toBe(13);
    });

    it('should generate random numbers', () => {
      const anotherArray = service.generateArray(12);
      expect(newArray).not.toEqual(anotherArray);
    });

    it('should return null when size is 0 or not entered', () => {
      const anotherArray = service.generateArray();
      expect(anotherArray).toBe(null);
    });
  });

  describe('findDuplicates', () => {
    const arrayA = [2, 74, 12, 13, 54, 13, 6, 7, 56, 74, 45];
    const arrayB = [2, 74, 12, 13, 54, 6, 7, 56, 45];
    const arrayC = [2, 74, 12, 13, 54, 13, 6, 7, 56, 74, 45, 13];

    it('should return duplicated numbers from array', () => {
      expect(service.findDuplicates(arrayA)).toEqual([13, 74]);
    });

    it('should return empty array when no duplicated', () => {
      expect(service.findDuplicates(arrayB)).toEqual([]);
    });

    it('should return as duplicated (only once) when is more than 2 times', () => {
      expect(service.findDuplicates(arrayA)).toEqual([13, 74]);
    });
    
  });
});