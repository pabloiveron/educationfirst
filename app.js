const service = require('./app/services/exerciseOne.service');

/**
 * @description Main function of the app. It generates an array of random numbers and print only the duplicated ones.
 * @param size Size of array. Entered as npm parameter (i.e. --size=10)
 */
const init = () => {
  const size = parseInt(process.env.npm_config_size, 10) || 1000000;
  
  console.time('Duplicates were found in');

  // Generate array
  const arr = service.generateArray(size);
  // Get duplicated elements array
  const duplicatedArr = service.findDuplicates(arr);
  // Printing duplicated arrays
  console.log(duplicatedArr);

  console.timeEnd('Duplicates were found in');
}

init();