# EducationFirst - Coding Challenge

This is a challenge excercise done by Pablo Ibanez as part of interview process for a senior front end position. 

## Challengue

The challenge is creating an array of random numbers with size based on a input parameter. This light app has been developed by using:

* [NodeJS] - Javascript run-time environment
* [Jest] - Testing framework

### Running it

Just type on console:

```sh
$ npm start --size=100
```
This will generate an array of 101 elements and it will find the duplicated ones. If size parameter is not entered size is 1.000.000 by default

### Test

There is a test set for the main service in the app.  The testing framework used Jest and it needs to be installed:

```sh
$ npm install
```
after that it can be run:

```sh
$ npm test --size=100
```